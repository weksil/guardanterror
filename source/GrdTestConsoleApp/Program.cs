﻿using System;
using SatansLittleHelper;

namespace GrdTestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var b = new B();
            Console.WriteLine("Created B w/ colors {0}", b.Colors);
            Console.ReadKey();
        }
    }
}
