﻿using System;

namespace SatansLittleHelper
{
    public struct MyColor
    {
        public int R;
        public int G;
        public int B;
    }

    public class B
    {
        public MyColor[] Colors;
        
        public B()
        {
            MyColor red = new MyColor() { R = 255, G = 0, B = 0 };
            MyColor white = new MyColor() { R = 255, G = 255, B = 255 };
            Colors = new MyColor[3];
            Colors[0] = white;
            Colors[1] = red;
            Colors[2] = white;
        }
    }
}
