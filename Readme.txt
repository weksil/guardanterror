При компиляции данного кода MSBuild 16 и последующего наложения защиты будет полученна ошибка:

Необработанное исключение: System.Reflection.TargetInvocationException: Адресат вызова создал исключение. ---> System.Exception: Invalid opcode was found
   в VM.VirtualMachine.Inject(String methodID)
   --- Конец трассировки внутреннего стека исключений ---
   в System.RuntimeMethodHandle.InvokeMethod(Object target, Object[] arguments, Signature sig, Boolean constructor)
   в System.Reflection.RuntimeMethodInfo.UnsafeInvokeInternal(Object obj, Object[] parameters, Object[] arguments)
   в System.Reflection.RuntimeMethodInfo.Invoke(Object obj, BindingFlags invokeAttr, Binder binder, Object[] parameters, CultureInfo culture)
   в SatansLittleHelper.B..ctor()
   в GrdTestConsoleApp.Program.Main(String[] args)

Если заменить класс на структуру, ошибка пропадёт:

``` 
public struct MyColor -> public class MyColor
```

Такая же ошибка воспроизводится во всех версиях MSBuild начиная с 14 версии включительно. В 12 версии ошибка не воспроизводится.